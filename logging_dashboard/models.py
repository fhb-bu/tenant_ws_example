from django.db import models
import logging
from django.contrib.auth.models import User
from django.db import connection
from tenant_ws.wsUtils import WebsocketUtility
from django.dispatch import receiver
from django.db.models.signals import post_save
from tenant_ws_logger.models import Client
from tenant_schemas.utils import get_public_schema_name
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

logger = logging.getLogger('django.request')


class LogMessages(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    loglevel = models.TextField(max_length=50, default="")
    date = models.TextField(max_length=50, default="")
    message = models.TextField(max_length=200, default="")

    @classmethod
    def getLogs(cls,username):
        returnList = []
        user = WebsocketDBHandler.getUser(username)
        messages = LogMessages.objects.all().filter(user=user)
        for message in messages:
            returnList.append({"user":username,
                               "loglevel":message.loglevel,
                               "date":message.date,
                               "message":message.message})
        return {"logmessages":[returnList]}

    @classmethod
    def addLog(cls,username,message):
        user = WebsocketDBHandler.getUser(username)
        log = LogMessages(user=user,
                          loglevel=message.get("loglevel"),
                          date=message.get("date"),
                          message=message.get("message"))
        log.save()

class Loggeduser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user")
    loggeduser = models.ForeignKey(User,related_name="loggeduser")

    @classmethod
    def getUser(cls,username, logusername):
        try:
            logUser= Loggeduser.objects.get(user=WebsocketDBHandler.getUser(username), loggeduser=WebsocketDBHandler.getUser(logusername))
        except ObjectDoesNotExist:
            logUser = None
        return logUser

    @classmethod
    def addUser(cls, username, logusername):
        user = WebsocketDBHandler.getUser(username)
        logged_user = WebsocketDBHandler.getUser(logusername)
        if not Loggeduser.getUser(username=username, logusername=logusername):
            Loggeduser(user=user, loggeduser=logged_user).save()

    @classmethod
    def delUser(cls, username, logusername):
        user = WebsocketDBHandler.getUser(username)
        logged_user = WebsocketDBHandler.getUser(logusername)
        Loggeduser.objects.get(user=user, loggeduser=logged_user).delete()

    @classmethod
    def checkUser(cls,username,logusername):
        user = WebsocketDBHandler.getUser(username)
        logged_user = WebsocketDBHandler.getUser(logusername)
        return Loggeduser.objects.get(user=user,loggeduser=logged_user).username

    @classmethod
    def getUsers(cls, username):
        user = WebsocketDBHandler.getUser(username)
        userlist = []
        for loggedUser in Loggeduser.objects.all().filter(user=user):
            userlist.append(loggedUser.username)
        return {"userlist":userlist}


# MODEL with additionally Userinfo

class UserInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    is_online = models.BooleanField(default=False)

    @classmethod
    def getOnlineState(cls,username):
        user = WebsocketDBHandler.getUser(username)
        userinfo = UserInfo.objects.get(user=user)
        return userinfo.is_online

    @classmethod
    def setOnlineState(cls,username):
        user = WebsocketDBHandler.getUser(username)
        userinfo = UserInfo.objects.get(user=user)
        if userinfo.is_online:
            userinfo.is_online = False
        else:
            userinfo.is_online = True
        userinfo.save()
        return userinfo.is_online


class WebsocketDBHandler():
    @classmethod
    def getUser(cls,username):
        if username == "":
            username=settings.ANONYMER_USERNAME
        user = None
        try:
            user = User.objects.get(username=username)
        except:
            if username == "":
                User.objects.create_user(settings.ANONYMER_USERNAME)
                user = User.objects.get(username=settings.ANONYMER_USERNAME)
        return user

    @classmethod
    def addUser(cls,username,email,password,isSuperuser,isStaff):
        user = User.objects.create_user(username=username,email=email,password=password)
        user.is_superuser = isSuperuser
        user.is_staff = isStaff
        user.save()

    @classmethod
    def generateUserList(cls,username):
        userlist = []
        for user in User.objects.all():
            logStatus = UserInfo.getOnlineState(user.username)
            were_logged = 0
            if Loggeduser.getUser(username=username,logusername=user.username):
                were_logged = 1
            userlist.append([{"username":user.username,
                             "is_online":logStatus,
                             "were_logged":were_logged}])
        return {"userlist":userlist}

    @classmethod
    def updateUserlist(cls,username,onlinestate,state):
        return {
            "username":username,
            "onlinestate":onlinestate,
            "state":state
        }

@receiver(post_save, sender=User)
def autocreateUserInfo(sender, created, instance, **kwargs):
    if created and (connection.schema_name != get_public_schema_name()):
        userinfo = UserInfo(user=instance)
        userinfo.save()

@receiver(post_save, sender=Client)
def autocreateUsers(sender, created, instance, **kwargs):
    if created:
        WebsocketUtility.setConnection(instance.schema_name)
        WebsocketDBHandler.addUser(
            settings.DEFAULT_SUPERUSER,
            '',
            settings.DEFAULT_PASSWORD,
            True,
            True)
        if (instance.schema_name != get_public_schema_name()):
            WebsocketDBHandler.addUser(
                settings.ANONYMER_USERNAME,
                '',
                settings.DEFAULT_PASSWORD,
                False,
                False)




