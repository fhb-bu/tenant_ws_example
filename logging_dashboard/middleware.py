from django.conf import settings
from django.contrib import auth
from datetime import datetime, timedelta


# Logout User after a while
# set with AUTO_LOGOUT_DELAY in settings

class AutoLogout:
  def process_request(self, request):
    #check if user is logged in
    if not request.user.is_authenticated():
      return
    try:
      if datetime.now() - datetime.strptime(request.session['last_touch'],"%Y-%m-%d %H:%M:%S") > timedelta( 0, settings.AUTO_LOGOUT_DELAY * 60, 0):
        auth.logout(request)
        del request.session['last_touch']
        return
    except KeyError:
      pass
    request.session['last_touch'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")



