class LogManager{
    constructor(){
        this.userList = [];
    }
    addUser(name){
        if (this.userList[name] == undefined){
            this.userList[name] = [];
        }
    }
    delUser(name){
        if (this.userList[name] != undefined){
            this.userList[name] = undefined;
        }
    }
    addMessage(name,type,message){
        if (this.userList[name][type] === undefined){
            this.userList[name][type] = [];
        }
        this.userList[name][type].push(message);
    }
    getMessage(name,type,id){
        var messages = "";
        if  (this.userList[name][type] !== undefined){
            for (let i = 0; i < this.userList[name][type].length; i++) {
                messages += this.userList[name][type][i] + " <br>";
            }
        }
        document.getElementById(id).innerHTML = messages;
        return messages;
    }
}
var log_manager = new LogManager();

