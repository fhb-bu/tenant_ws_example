class GUIprototyp{
    constructor(type){
        this.base = document.createElement(type);
    }
    toHTML(){
        return this.base;
    }
}


class GUIuseroverview extends GUIprototyp{
    constructor(userlist){
        super("div");
        this.base.setAttribute("id","userlist");
        this.T_Table = document.createElement("table");
        this.T_Table.setAttribute("class","table");
        this.T_Table.appendChild(this.generateT_Header());
        this.generateUserlist(userlist);
        this.base.appendChild(this.T_Table);
    }
    generateT_Header(){
        var T_Row = document.createElement("tr");
        var H_User = document.createElement("th");
        H_User.innerHTML = "USER";
        var H_Online = document.createElement("th");
        H_Online.innerHTML = "ONLINE";
        var H_State = document.createElement("th");
        H_State.innerHTML = "STATE";
        T_Row.appendChild(H_User);
        T_Row.appendChild(H_Online);
        T_Row.appendChild(H_State);
        return T_Row
    }
    generateUserlist(userlist){
        for (var i = 0; i < userlist.length; i++) {
              if (userlist[i][0].were_logged === 1){
                handleWS.send_loggingstatus(userlist[i][0].username,userlist[i][0].were_logged)
            }
        this.T_Table.appendChild(new GUIuserentry (userlist[i][0].username
            , userlist[i][0].is_online
            , userlist[i][0].were_logged).toHTML());
        }
    }
}

class GUIuserentry extends GUIprototyp{
    constructor(username, is_online, were_logged){
        super("tr");
        var userid = 'userOverview_' + username;
        this.base.setAttribute("id",userid);
        var userColumn = document.createElement("td");
        userColumn.innerHTML = username;
        this.base.appendChild(userColumn);
        this.base.appendChild(new GUIonlineState(is_online).toHTML());
        this.base.appendChild(new GUIstate(username,were_logged).toHTML());
    }
}


class GUIonlineState extends GUIprototyp{
    constructor(is_online){
        super("td");
        var buttonOnlineClass = "btn-danger";
        var buttonOnlineText = "Offline";
          if (is_online){
            buttonOnlineClass = "btn-success";
            buttonOnlineText = "Online";
        }
        this.base.setAttribute("class",buttonOnlineClass);
        this.base.innerHTML = buttonOnlineText;
    }
}

class GUIstate extends GUIprototyp{
    constructor(username,were_logged){
        super("td");
        var loggingState = "inactiv";
        var buttonStateClass = "btn-danger userlist_tableentry";
        var state = 0;
        if (were_logged == 1){
            loggingState = "activ";
            buttonStateClass = "btn-info userlist_tableentry"
            state = 1;
        }
        this.base.setAttribute("class",buttonStateClass);
        this.base.setAttribute("state",state);
        this.madeState(username,were_logged);
        this.base.innerHTML = loggingState;
    }
    madeState(username, state){
        var loggingState = 1;
        if (state == 1){
            status = "activ";
            loggingState = 0;
        }
        var onclickFunction = 'handleWS.send_loggingstatus("' + username + '",' + loggingState + ')';
        this.base.setAttribute("onclick",onclickFunction);
    }
}

class GUIgriditem extends GUIprototyp{
    constructor(number){
        super("div");
        this.base.setAttribute('id',"gridItem"+number);
        this.base.setAttribute('placeholder',"true");
        this.base.setAttribute('class',"col-md-4 gridItemStyle");
        this.base.setAttribute('ondrop',"DragAndDrop.drop(event)");
        this.base.setAttribute('ondragover',"DragAndDrop.allowDrop(event)");
    }
}

class GUIwidget extends GUIprototyp{
    constructor(name){
        super("div");
        this.base.setAttribute('class',"dragItem");
        this.base.setAttribute('draggable',"true");
        this.base.setAttribute('ondragstart',"DragAndDrop.drag(event)");
        this.base.setAttribute('id',name);
        var logWidget = document.createElement("div")
        logWidget.setAttribute("class","panel panel-default");
        var header = document.createElement("div");
        header.setAttribute("class","panel-heading");
        header.setAttribute("align","center");
        header.innerHTML = name;
        logWidget.appendChild(header);
        var buttons = document.createElement("div");
        buttons.setAttribute("align","center");
        buttons.setAttribute("class","buttons");
        buttons.appendChild(new GUIlogbutton(name,"ERROR").toHTML());
        buttons.appendChild(new GUIlogbutton(name,"WARNING").toHTML());
        buttons.appendChild(new GUIlogbutton(name,"INFO").toHTML());
        logWidget.appendChild(buttons);
        var textArea = document.createElement("div");
        var textAreaId = name+"_widget_log_"+"logViewID";
        textArea.setAttribute('id',textAreaId);
        textArea.setAttribute('class',"logViewItem");
        logWidget.appendChild(textArea);
        this.base.appendChild(logWidget);
        }
}

class GUIlogbutton extends GUIprototyp{
    constructor(name,type){
        super("button");
  var typeName = "";
  var label = ""
  if (type == "ERROR"){
    typeName = "btn btn-danger";
    label = "Error";
  }
  if (type == "WARNING"){
    typeName = "btn btn-warning";
    label = "Warning";
  }
  if (type == "INFO"){
    typeName = "btn btn-info";
    label = "Info";
  }
  this.base.setAttribute('class',typeName);
  this.base.innerHTML = label;
  this.base.setAttribute('type',"button");
  var logViewID = name+"_widget_log_logViewID";
  var onclickFunction = "log_manager.getMessage("+"'"+name+"','"+type+"','"+logViewID+"')";
  this.base.setAttribute('onclick',onclickFunction);
    }
}

class Controller{
        constructor(){
    }
    static addWidget(name,widgetName){
        var pos = document.getElementById(Controller.getFreePos());
        pos.setAttribute('placeholder',"false");
        pos.appendChild(new GUIwidget(name).toHTML());
    }
    static deleteWidget(name) {
        var widget = document.getElementById(name);
        var pos = widget.parentElement;
        pos.setAttribute('placeholder', "true");
        pos.innerHTML = "";
    }
    static getFreePos(){
        var children = document.getElementById("grid").childNodes;
        for (let i=0; i<children.length;i++){
            if (children[i].getAttribute("placeholder") == "true"){
                return children[i].getAttribute("id");
            }
        };
    }
    static generateGrid(size){
        var grid = document.getElementById("grid");
        for (var i=0; i<size;i++){
            grid.appendChild(new GUIgriditem(i).toHTML());
        }
    }
    static addLogMessage(username,level,date,text){
        var logViewID = username+"_widget_log_logViewID";
        if (document.getElementById(logViewID)){
            document.getElementById(logViewID).innerHTML += text;
        }
        log_manager.addMessage(username,level,text);
    }
    static addMessages(messages) {
        for (var i = 0; i < messages[0].length; i++) {
            let msg = messages[0][i];
            Controller.addLogMessage(msg.user, msg.loglevel, msg.date, msg.message);
        }
    }
    static updateUserOverview(username, is_online, were_logged) {
        var userid = 'userOverview_' + username;
        var parent = document.getElementById(userid)
        var userEntry = parent.childNodes;
            if (were_logged == -1){
                were_logged = document.getElementById(userid).childNodes[2].getAttribute("state");
            }
            parent.replaceChild(new GUIonlineState(is_online).toHTML(),userEntry[1]);
            parent.replaceChild(new GUIstate(username,were_logged).toHTML(),userEntry[2]);
        }
    static generateUserOverview(userlist) {
        if (document.getElementById("sidemenu").childNodes.length == 0){
            document.getElementById("sidemenu").appendChild(new GUIuseroverview(userlist).toHTML());
        }
    }
}

window.onload = function () {
    Controller.generateGrid(9);
};