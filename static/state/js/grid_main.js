class GUIprototyp{
    constructor(type){
        this.base = document.createElement(type);
    }
    toHTML(){
        return this.base;
    }
}

class GUIbuttom extends GUIprototyp{
    constructor(btClass,text,btFunction,id){
        super("button");
        this.base.setAttribute("class",btClass);
        this.base.setAttribute("type","button");
        this.base.setAttribute("onclick",btFunction);
        if (id){
          this.base.setAttribute("id",id);
        }
        this.base.innerHTML = text;
    }
}

class GUIgriditem extends GUIprototyp{
    constructor(number){
        super("div");
        this.base.setAttribute('id',"gridItem"+number);
        this.base.setAttribute('placeholder',"true");
        this.base.setAttribute('class',"col-md-4 gridItemStyle");
        this.base.setAttribute('ondrop',"DragAndDrop.drop(event)");
        this.base.setAttribute('ondragover',"DragAndDrop.allowDrop(event)");
    }
}

class GUIwidgetBase extends GUIprototyp {
    constructor(name,id) {
        super("div");
        this.base.setAttribute('class', "dragItem");
        this.base.setAttribute('draggable', "true");
        this.base.setAttribute('ondragstart', "DragAndDrop.drag(event)");
        this.base.setAttribute('id', id);
        this.mainwidget = document.createElement("div");
        this.mainwidget.setAttribute("class","panel panel-primary");
        this.setHeader(name);
        this.base.appendChild(this.mainwidget);
    }
    setHeader(name){
        let header = document.createElement("div");
        header.setAttribute("class","panel-heading");
        header.setAttribute("align","center");
        header.innerHTML = name;
        this.mainwidget.appendChild(header);
    }
    setBody(childs){
       let body = document.createElement("div");
       body.setAttribute("class","panel-body");
       for (let i=0; i<childs.length;i++){
            body.appendChild(childs[i]);
        }
        this.mainwidget.appendChild(body);
    }
}

class GUIstateWidget extends GUIwidgetBase{
    constructor(name,id) {
        super(name, id);
        this.setBody([this.setCPUTable(),this.setMEMORYTable()])
    }
    setTableHead(name){
        let header = document.createElement("thead");
        let tr = document.createElement("tr");
        let th = document.createElement("th");
        th.innerHTML = name;
        tr.appendChild(th);
        header.appendChild(tr);
        return header
    }
    setTableRow(name,id){
        let tr = document.createElement("tr");
        let td1 = document.createElement("td");
        td1.innerHTML = name;
        let td2 = document.createElement("td");
        td2.setAttribute("id",id);
        tr.appendChild(td1);
        tr.appendChild(td2);
        return tr
    }
    setCPUTable(){
        let table = document.createElement("table");
        table.setAttribute("class","table");
        table.appendChild(this.setTableHead("CPU"));
        table.appendChild(this.setTableRow("Anzahl","CPUNUMBER"));
        table.appendChild(this.setTableRow("Auslastung","CPUUSAGE"));
        return table
    }
     setMEMORYTable(){
        let table = document.createElement("table");
        table.setAttribute("class","table");
        table.appendChild(this.setTableHead("MEMORY"));
        table.appendChild(this.setTableRow("Gesamt","MEMORYTOTAL"));
        table.appendChild(this.setTableRow("Verfügbar","MEMORYFREE"));
        table.appendChild(this.setTableRow("Auslastung","MEMORYUSAGE"));
        return table
    }
}

class GUIsensorWidget extends GUIwidgetBase{
    constructor(name,id) {
        super(name, id);
        this.setBody([this.setSensorTable()])
    }
    setTableHead(name,id){
        let header = document.createElement("thead");
        let tr = document.createElement("tr");
        let th = document.createElement("th");
        th.setAttribute("id",id);
        th.innerHTML = name;
        tr.appendChild(th);
        header.appendChild(tr);
        return header
    }
    setTableRow(name,id){
        let tr = document.createElement("tr");
        let td1 = document.createElement("td");
        td1.innerHTML = name;
        let td2 = document.createElement("td");
        td2.setAttribute("id",id);
        tr.appendChild(td1);
        tr.appendChild(td2);
        return tr
    }
    setSensorTable(){
        let table = document.createElement("table");
        table.setAttribute("class","table");
        table.appendChild(this.setTableHead("Sensor","SENSORTYPE"));
        table.appendChild(this.setTableRow("Temperatur","SENSORTEMPERATURE"));
        return table
    }
}

class GUIprocesslistWidget extends GUIwidgetBase{
    constructor(name,id) {
        super(name, id);
        this.setBody([this.setTable()])
    }
    setTable(){
        let table = document.createElement("table");
        table.setAttribute("class","table header-fixed");
        table.setAttribute("id","PROCESSLIST");
        return table
    }
}

class Controller{
        constructor(){
    }
    static addWidget(name,widgetName){
        let pos = document.getElementById(Controller.getFreePos());
        pos.setAttribute('placeholder',"false");
        if (widgetName === "Systemstatus"){
            pos.appendChild(new GUIstateWidget(name,widgetName).toHTML())
        }
        if (widgetName === "Sensoranzeige"){
            pos.appendChild(new GUIsensorWidget(name,widgetName).toHTML())
        }
        if (widgetName === "Prozessliste"){
            pos.appendChild(new GUIprocesslistWidget(name,widgetName).toHTML())
        }


    }
    static deleteWidget(name) {
        let widget = document.getElementById(name);
        let pos = widget.parentElement;
        pos.setAttribute('placeholder', "true");
        pos.innerHTML = "";
    }
    static getFreePos(){
        let children = document.getElementById("grid").childNodes;
        for (let i=0; i<children.length;i++){
            if (children[i].getAttribute("placeholder") === "true"){
                return children[i].getAttribute("id");
            }
        }
    }
    static addMenu(){
        let menulist = document.getElementById("sidemenu");
        menulist.appendChild(
            new GUIbuttom("btn btn-danger","Systemstatus","Controller.handleControlButton('SYSTEMSTATE')","SYSTEMBUTTON").toHTML()
        );
        menulist.appendChild(
            new GUIbuttom("btn btn-danger","Sensordaten","Controller.handleControlButton('SENSORSTATE')","SENSORBUTTON").toHTML()
        );
        menulist.appendChild(
            new GUIbuttom("btn btn-danger","Prozessliste","Controller.handleControlButton('PROCESSLIST')","PROCESSBUTTON").toHTML()
        );
    }
    static generateGrid(size){
        let grid = document.getElementById("grid");
        for (let i=0; i<size;i++){
            grid.appendChild(new GUIgriditem(i).toHTML());
        }
    }
    static changeButton(id){
        let button = document.getElementById(id);
        let classname = button.className;
        if (classname === "btn btn-danger"){
            button.className = "btn btn-info";
            return true
        }
        else{
              button.className = "btn btn-danger";
            return false
        }
    }

    static handleControlButton(type){
        let button = undefined;
        let widget = undefined;
        let widget_name = undefined;
        if (type === "SENSORSTATE"){
            button = "SENSORBUTTON";
            widget = "SENSOR";
            widget_name = "Sensoranzeige";
        }
        if (type === "SYSTEMSTATE"){
            button = "SYSTEMBUTTON";
            widget = "STATE";
            widget_name = "Systemstatus";
        }
        if (type === "PROCESSLIST"){
            button = "PROCESSBUTTON";
            widget = "PROCESS";
            widget_name = "Prozessliste";
        }
        if (Controller.changeButton(button)){
            Controller.addWidget(widget,widget_name);
            handleWS.send_start(type);
        }
        else{
            Controller.deleteWidget(widget_name);
            handleWS.send_stop(type);
        }
    }

    static handleSensorData(data){
        document.getElementById("SENSORTYPE").innerHTML = data.SENSOR;
        document.getElementById("SENSORTEMPERATURE").innerHTML = data.TEMPERATURE;
        console.log(data);
    }

    static handleSystemData(data){
        document.getElementById("CPUNUMBER").innerHTML = data.CPU.number;
        document.getElementById("CPUUSAGE").innerHTML = data.CPU.usage;
        document.getElementById("MEMORYTOTAL").innerHTML = data.MEMORY.available;
        document.getElementById("MEMORYFREE").innerHTML = data.MEMORY.free;
        document.getElementById("MEMORYUSAGE").innerHTML = data.MEMORY.percent;
    }

    static handleProcesslistData(data){
        let table = document.getElementById("PROCESSLIST");
        table.innerHTML = "";
        let thead = document.createElement("thead");
        let tr_head = document.createElement("tr");
        tr_head.appendChild(Controller.generateProcesslistEntry("ID","th"));
/**        tr_head.appendChild(Controller.generateProcesslistEntry("Time","th"));**/
        tr_head.appendChild(Controller.generateProcesslistEntry("Name","th"));
        thead.appendChild(tr_head);
        table.appendChild(thead);
        let tbody = document.createElement("tbody");
        for (let i=0; i<data.PROCESSLIST.length;i++){
            let tr = document.createElement("tr");
            tr.appendChild(Controller.generateProcesslistEntry(data.PROCESSLIST[i][0],"td"));
 /**           tr.appendChild(Controller.generateProcesslistEntry(data.PROCESSLIST[i][1],"td"));**/
            tr.appendChild(Controller.generateProcesslistEntry(data.PROCESSLIST[i][2],"td"));
            tbody.appendChild(tr);
        }
        table.appendChild(tbody);
    }
    static generateProcesslistEntry(entry,type){
        let td = document.createElement(type);
        td.innerHTML = entry;
        return td
    }
}

window.onload = function () {
    Controller.generateGrid(9);
    Controller.addMenu();
};