from django.contrib.auth.decorators import login_required
import functools
from copy import copy


def usingWebsockets(func):
    @login_required
    @functools.wraps(func)
    def inner(request, *args, **kwargs):
        request.session['info'] = {
            'tenant':request.tenant.schema_name,
            'user':request.user.username,
            'path':request.path.replace("/","")
        }
        return func(request, *args, **kwargs)
    return inner


def usingSessionData(func):
    @functools.wraps(func)
    def inner(message, *args, **kwargs):
        session = copy(message.http_session.__getitem__("info"))
        message.channel_session['info'] = {'tenant': session["tenant"],
                                           'user': session["user"],
                                           'path': session["path"]}
        return func(message, *args, **kwargs)
    return inner