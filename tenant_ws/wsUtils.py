from django.db import connection
from tenant_schemas.utils import get_tenant_model

class WebsocketUtility:
    @classmethod
    def setConnection(cls,tenantname):
        connection.set_tenant(get_tenant_model()
                              .objects
                              .get(schema_name=tenantname))
