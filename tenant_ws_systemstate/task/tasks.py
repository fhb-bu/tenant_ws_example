from tenant_ws.celery.ws_tasks import WebsocketTask

from tenant_ws_systemstate.sensor import SystemStats, SensorStats, ProcessList
from tenant_ws_systemstate.models import Sensor

class SystemStatsTask(WebsocketTask):
    def work(self,tenant,user,path,params):
        return SystemStats.getStats()

class SensorTask(WebsocketTask):
    def work(self,tenant,user,path,params):
        return SensorStats.getTemperatur(
            Sensor.getSensorname().get("sensorname")
        )

class ProcessTask(WebsocketTask):
    def work(self,tenant,user,path,params):
        return ProcessList.getProcessList(user)
