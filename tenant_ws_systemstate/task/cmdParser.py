import subprocess
class CMD:
    @classmethod
    def getOutput(cls, commands):
        try:
            if type(commands[0]) == str:
                commands = [commands]
            preProcess = None
            for i, command in enumerate(commands):
                if preProcess:
                    p = subprocess.Popen(command, stdin=preProcess.stdout, stdout=subprocess.PIPE)
                    preProcess.stdout.close()
                    preProcess.kill()
                else:
                    p = subprocess.Popen(command, stdout=subprocess.PIPE)
                preProcess = p
            output = p.communicate()[0].decode("utf-8")
            p.kill()
            return output
        except TypeError:
            print("Fehler bei der Eingabe")
        except FileNotFoundError:
            print("Befehl nicht gefunden")
