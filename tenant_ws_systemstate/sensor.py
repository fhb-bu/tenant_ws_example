from tenant_ws_systemstate.task.cmdParser import CMD
import psutil
import random
import re

IS_VM = True

class SensorStats:
    @classmethod
    def getTemperatur(cls, sensorTyp):
        if not IS_VM:
            output = CMD.getOutput([["sensors"], ["grep", "-A2", sensorTyp], ["grep", "Package"]])
            strStart = output.find("+")
            strStop = output.find("°")
            strTemp = output[strStart + 1:strStop]
        else:
            strTemp = str(random.randint(1,50))
        return {
            "SENSOR": sensorTyp,
            "TEMPERATURE": strTemp,
        }

class SystemStats:
    @classmethod
    def getCPUUsage(cls):
        cpu_usage = psutil.cpu_percent(interval=1, percpu=True)
        cpu_usage_clean = " ".join(
            SystemStats.translateUsage(
                cpu_usage
            )
        )
        cpu_number = len(cpu_usage)
        return {
            "number" : cpu_number,
            "usage" : cpu_usage_clean
        }
    @classmethod
    def getMemoryUsage(cls):
        memory = psutil.virtual_memory()
        return {
            "available" : memory.available,
            "free" : memory.free,
            "percent" : memory.percent
        }
    @classmethod
    def getStats(cls):
        return {
            "CPU" : SystemStats.getCPUUsage(),
            "MEMORY" : SystemStats.getMemoryUsage()
    }
    @classmethod
    def translateUsage(cls,usage):
        rtnlist = []
        for item in usage:
            rtnlist.append(str(item))
        return rtnlist

class ProcessList:
    exp = re.compile('(\d).*(\d\d:\d\d:\d\d)\s(.\w*)')
    @classmethod
    def getProcessList(cls,user):
        output = CMD.getOutput(["ps","-U",user])
        user_exist = True
        if output.find("Fehler") == -1:
            output =ProcessList.exp.findall(output)
        else:
            user_exist = False
            output = []
        return {
                "PROCESSLIST":output,
                "USER_EXIST": user_exist,
        }


if __name__ == '__main__':
    print(ProcessList.getProcessList("root"))
