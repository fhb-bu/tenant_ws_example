from django.shortcuts import  render_to_response
from  tenant_ws.decorators import usingWebsockets
from tenant_ws_systemstate.models import Sensor

def getRole(request):
    if request.user.is_superuser:
        role = "Superuser"
    elif request.user.is_staff:
        role = "Staff"
    else:
        role = "User"
    return role

@usingWebsockets
def StateView(request):
    sitename = "tenants/state/state.html"
    context=({'title' : 'Index',
              'username': request.user.username,
              'role': getRole(request)
              })
    response = render_to_response(sitename,context)
    return response

def SensorAdminView(request):
    sitename = "tenants/state/sensor_admin.html"
    context=({'title' : 'Sensoradmin'})
    response = render_to_response(sitename,context)
    if request.user.is_superuser:
        if request.method == 'POST':
            try:
                sensorname = request.POST['sensorname']
                Sensor.setSensorname(sensorname)
            except:
                pass
    return response
