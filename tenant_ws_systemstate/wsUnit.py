from channels import Group

from tenant_ws.wsSkeleton import wsSkeleton, wsGroups
from tenant_ws.celery.taskmanager import TaskManager

class wsSystemState(wsSkeleton):
    def __init__(self):
        wsSkeleton.__init__(self)
        self.path = "state"
        self.shouldOverwriting = False
        self.incoming = {
            "start":{
                "task":str
                },
            "stop":{
                "task":str
                }
            }
        self.outgoing = {
            "temperature":{
                "SENSOR":str,
                "TEMPERATURE":str
                },
            "state":{
                "CPU":dict,
                "MEMORY":dict,
            },
            "processes":{
                "PROCESSLIST":list,
                "USER_EXIST":bool,
            }
        }
        self.groups = {
            "status" : wsGroups(False,True,True),
            "command": wsGroups(False, True, True),
        }

    def onConnect(self,reply_channel,info_channel):
        pass

    def sendMessage(self,cmd,message,group,tenant=None,username=None,celery=None):
        if not celery:
            group = self.generateGroupName(group,tenant,username)
        Group(group).send(
            self.generateJSON(cmd,self.path,message),
            immediately=True)

    def handleMessage(self,cmd,message,reply_channel,info_channel):
        if cmd == "start":
            TaskManager.addTask(
                reply_channel,
                message.get("task"),
                {
                    "tenant": info_channel.get("tenant"),
                    "user": info_channel.get("user"),
                    "path": info_channel.get("path"),
                },
                {}
            )
        if cmd == "stop":
            TaskManager.delTask(
                reply_channel,
                message.get("task"),
                {
                    "tenant": info_channel.get("tenant"),
                    "user": info_channel.get("user"),
                },
            )