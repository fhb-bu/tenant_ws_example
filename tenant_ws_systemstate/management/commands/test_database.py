from django.core.management.base import BaseCommand
from tenant_ws_logger.models import Client
from django.conf import settings
from tenant_schemas.utils import get_public_schema_name
from django.core.management import call_command
import os
import shutil

import json
from datetime import datetime, timedelta


class Command(BaseCommand):
    def __init__(self):
        BaseCommand.__init__(self)

    def add_arguments(self, parser):
        parser.add_argument(
            'mode',
            metavar='options',
        )
    def handle(self, *args, **options):
        try:
            if options["mode"] == "add":
                self.create()
            elif options["mode"] == "del":
                self.delete()
        except Exception as err:
            print("bla")

    def create(self):
        schedule, created = IntervalSchedule.objects.get_or_create(
            every = 10,
            period = IntervalSchedule.SECONDS,
        )
        PeriodicTask.objects.create(
            interval=schedule,
            name = "peter1",
            task="tenant_ws_systemstate.tasks.Peter",
            kwargs=json.dumps({'tenant':'bla',"user":"bla"}),
        )

    def delete(self):
        PeriodicTask.objects.get(name="peter1").delete()