from django.db import models

DEFAULT_SENSORNAME = ""

class Sensor(models.Model):
    sensorname = models.TextField(max_length=255, default="",primary_key=True)

    @classmethod
    def getSensorname(cls):
        sensor = Sensor.objects.all()
        sensorname = DEFAULT_SENSORNAME
        if len(sensor) == 1:
            sensorname = sensor[0].sensorname
        return {"sensorname":sensorname}

    @classmethod
    def setSensorname(cls,sensorname):
        try:
            sensor = Sensor.objects.all()
            sensor = sensor[0]
            sensor.sensorname = sensorname
            sensor.save()
        except:
            Sensor(sensorname=sensorname).save()