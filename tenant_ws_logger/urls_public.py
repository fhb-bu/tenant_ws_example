from django.conf.urls import url
from tenant_ws_logger.view import LoginView, AdminIndexView,IndexView


urlpatterns = [
    url(r'^$', IndexView),
    url(r'login/', LoginView,name="LoginView"),
    url(r'Index_Admin/', AdminIndexView,name="admin"),
]

