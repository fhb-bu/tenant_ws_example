from tenant_ws_logger.models import Client
from django.contrib.auth import authenticate, login
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect
from django.shortcuts import render_to_response


def LoginView(request):
    sitename = "public/admin/login.html"
    context=({'title' : 'Login'})
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if not user:
            context.update({"Error":"Wrong Login Data"})
        if user is not None and user.is_superuser:
            login(request, user)
            return redirect('/Index_Admin/')
    response = render_to_response(sitename,context)
    return response


def IndexView(request):
    sitename = "public/base/index.html"
    context=({'title' : 'Index'})
    response = render_to_response(sitename,context)
    return response


# Administrator Interface to manage Tenants
def AdminIndexView(request):
    action = None
    context = None
    sitename = None
    if request.user.is_superuser:
        if request.method == 'POST':
            try:
                action = request.POST['action']
            except:
                pass
        if action:
            if action == "addView":
                sitename = "public/admin/tenants_add.html"
                context = {"title" : "adding Tenant"}

            else:
                schemaName = None
                name = None
                description = None
                try:
                    schemaName = request.POST['schemaName']
                    name = request.POST['name']
                    description = request.POST['description']
                    context = {"tenant":schemaName}

                except:
                    context.update({"error" : "fail to read tenantsdata"})
                if action == "addTenant":
                    sitename = "public/admin/tenants_added.html"
                    context.update({"title" : "Tenant added"})
                    try:
                        Client.addTenant(schemaName,name,description)
                    except:
                        context.update({"error" : "fail to creating tenant"})
                elif action == "delTenant":
                    sitename = "public/admin/tenants_deleted.html"
                    context.update({"title" : "Tenant deleted"})
                    try:
                        Client.delTenant(schemaName,name,description)
                    except:
                        context.update({"error" : "fail to delete tenant"})
        else:
            sitename = "public/admin/tenants_index.html"
            tenantList=Client.getTenants()
            context={'title' : 'Tenants Index',
                  'tenantlist': tenantList}
        response = render_to_response(sitename,context)
        return response
    else:
        raise PermissionDenied