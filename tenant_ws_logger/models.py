from django.db import models
from tenant_schemas.models import TenantMixin
from tenant_ws_logger import settings
from tenant_schemas.utils import get_public_schema_name
from tenant_schemas.signals import post_schema_sync

from django.db import connection

class Client(TenantMixin):
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=200)
    created_on = models.DateField(auto_now_add=True)

    auto_drop_schema = True
    auto_create_schema = True

    # fix for creation
    def save(self, verbosity=1, *args, **kwargs):
        is_new = self.pk is None

        if is_new and connection.schema_name != get_public_schema_name():
            raise Exception("Can't create tenant outside the public schema. "
                            "Current schema is %s." % connection.schema_name)
        elif not is_new and connection.schema_name not in (self.schema_name, get_public_schema_name()):
            raise Exception("Can't update tenant outside it's own schema or "
                            "the public schema. Current schema is %s."
                            % connection.schema_name)
        if is_new and self.auto_create_schema:
            try:
                self.create_schema(check_if_exists=True, verbosity=verbosity)
            except:
                # We failed creating the tenant, delete what we created and
                # re-raise the exception
                self.delete(force_drop=True)
                raise
            else:
                post_schema_sync.send(sender=TenantMixin, tenant=self)

        super(TenantMixin, self).save(*args, **kwargs)


    @classmethod
    def getTenants(cls):
        returnList = []
        tenantsList = Client.objects.all()
        for tenant in tenantsList:
            if not tenant.schema_name == get_public_schema_name():
                returnList.append(tenant)
        return returnList

    @classmethod
    def addTenant(cls,schema_name,name,description):
        domain_url = schema_name+"."+settings.BASE_URL
        try:
            Client(domain_url=domain_url,schema_name=schema_name,name=name,description=description).save()
        except:
            print("Can't create Tenant: ",schema_name)

    @classmethod
    def delTenant(cls,schema_name,name,description):
        Client.objects.get(schema_name=schema_name).delete()






