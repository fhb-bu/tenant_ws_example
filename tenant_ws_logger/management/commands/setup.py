from django.core.management.base import BaseCommand
from tenant_ws_logger.models import Client
from django.conf import settings
from tenant_schemas.utils import get_public_schema_name
from django.core.management import call_command
import os
import shutil

MIGRATION_DIR_NAME = "migrations"
INSTALL_LOG_FILE = "install.log"

class Command(BaseCommand):
    def __init__(self):
        BaseCommand.__init__(self)
        self.appsToMigrate = []
    def handle(self, *args, **options):
        try:
            self.checkFileSystem()
            self.doMigrations()
            self.createPublic()
            self.stdout.write(self.style.SUCCESS("Setup was success"))
        except Exception as err:
            self.writeLogFile(err)
            self.stdout.write(self.style.ERROR("Setup has failed, please look at the Logfile {}".format(INSTALL_LOG_FILE)))

    def doMigrations(self):
        for app in self.appsToMigrate:
            call_command('makemigrations',app)
        call_command('migrate_schemas','--shared')

    def checkFileSystem(self):
        if os.path.exists(os.path.join(settings.BASE_DIR,MIGRATION_DIR_NAME)):
            shutil.rmtree(os.path.join(settings.BASE_DIR,MIGRATION_DIR_NAME))
        for dir in os.listdir(settings.BASE_DIR):
            if os.path.exists(os.path.join(settings.BASE_DIR, dir, MIGRATION_DIR_NAME)):
                shutil.rmtree(os.path.join(settings.BASE_DIR, dir, MIGRATION_DIR_NAME))
            if os.path.exists(os.path.join(os.path.join(settings.BASE_DIR, dir, "models.py"))):
                self.appsToMigrate.append(dir)

    def createPublic(self):
        Client(schema_name=get_public_schema_name(),
               name='',
               domain_url=settings.DEFAULT_PUBLIC_URL,
               description="Public"
               ).save()

    def writeLogFile(self,err):
        try:
            file = open(os.path.join(settings.BASE_DIR,INSTALL_LOG_FILE),"w")
            file.write(err)
            file.close()
        except:
            print(err)
